local modname = "voxeload"

local top_axis = {
    x = 0,
    y = 1,
    z = 0,
}

local rotation_around_top = function(
    top
)
    return {
        x = {
            x = top.x * top.x,
            y = top.x * top.y - top.z,
            z = top.x * top.z + top.y,
        },
        y = {
            x = top.y * top.x + top.z,
            y = top.y * top.y,
            z = top.y * top.z - top.x,
        },
        z = {
            x = top.z * top.x - top.y,
            y = top.z * top.y + top.x,
            z = top.z * top.z,
        },
    }
end

local face_axis = {
}

face_axis[
    0
] = {
    x = {
        x = 1,
        y = 0,
        z = 0,
    },
    y = {
        x = 0,
        y = 1,
        z = 0,
    },
    z = {
        x = 0,
        y = 0,
        z = 1,
    },
}

face_axis[
    1
] = {
    x = {
        x = 0,
        y = 0,
        z = -1,
    },
    y = {
        x = 1,
        y = 0,
        z = 0,
    },
    z = {
        x = 0,
        y = 1,
        z = 0,
    },
}

face_axis[
    2
] = {
    x = {
        x = 1,
        y = 0,
        z = 0,
    },
    y = {
        x = 0,
        y = 0,
        z = 1,
    },
    z = {
        x = 0,
        y = -1,
        z = 0,
    },
}

face_axis[
    3
] = {
    x = {
        x = 0,
        y = 1,
        z = 0,
    },
    y = {
        x = -1,
        y = 0,
        z = 0,
    },
    z = {
        x = 0,
        y = 0,
        z = 1,
    },
}

face_axis[
    4
] = {
    x = {
        x = 0,
        y = -1,
        z = 0,
    },
    y = {
        x = 1,
        y = 0,
        z = 0,
    },
    z = {
        x = 0,
        y = 0,
        z = 1,
    },
}

face_axis[
    5
] = {
    x = {
        x = -1,
        y = 0,
        z = 0,
    },
    y = {
        x = 0,
        y = -1,
        z = 0,
    },
    z = {
        x = 0,
        y = 0,
        z = 1,
    },
}

local facedir_box = function (
    original_box,
    facedir
)
    local pr = facedir % 4
    local pa = (
        facedir - pr
    ) / 4
    local axis_direction = face_axis[
        pa
    ]
    local rtop = {
    }
    for k, v in pairs(
        axis_direction
    ) do
        rtop[
            k
        ] = vector.dot(
            v,
            top_axis
        )
    end
    local rotation = rotation_around_top(
        rtop
    )
    local p1 = {
        x = original_box[
            1
        ],
        y = original_box[
            2
        ],
        z = original_box[
            3
        ],
    }
    local p2 = {
        x = original_box[
            4
        ],
        y = original_box[
            5
        ],
        z = original_box[
            6
        ],
    }
    local rp1 = {
    }
    for k, v in pairs(
        axis_direction
    ) do
        rp1[
            k
        ] = vector.dot(
            v,
            p1
        )
    end
    local rp2 = {
    }
    for k, v in pairs(
        axis_direction
    ) do
        rp2[
            k
        ] = vector.dot(
            v,
            p2
        )
    end
    p1 = vector.copy(
        rp1
    )
    p2 = vector.copy(
        rp2
    )
    for i = 1, pr do
        for k, v in pairs(
            rotation
        ) do
            rp1[
                k
            ] = vector.dot(
                v,
                p1
            )
        end
        for k, v in pairs(
            rotation
        ) do
            rp2[
                k
            ] = vector.dot(
                v,
                p2
            )
        end
        p1 = vector.copy(
            rp1
        )
        p2 = vector.copy(
            rp2
        )
    end
    local new_box = {
        math.min(
            p1.x,
            p2.x
        ),
        math.min(
            p1.y,
            p2.y
        ),
        math.min(
            p1.z,
            p2.z
        ),
        math.max(
            p1.x,
            p2.x
        ),
        math.max(
            p1.y,
            p2.y
        ),
        math.max(
            p1.z,
            p2.z
        ),
    }
    return new_box
end

local split_boxes = function (
    resolution
)
    local result = {
    }
    for x = 1, resolution do
        for y = 1, resolution do
            for z = 1, resolution do
                result[
                    "x" .. x .. "y" .. y .. "z" .. z
                ] = {
                    (
                        x - 1
                    ) / resolution - 0.5,
                    (
                        y - 1
                    ) / resolution - 0.5,
                    (
                        z - 1
                    ) / resolution - 0.5,
                    x / resolution - 0.5,
                    y / resolution - 0.5,
                    z / resolution - 0.5,
                }
            end
        end
    end
    return result
end

local box_intersect = function (
    one,
    other
)
    return {
        math.max(
            one[
                1
            ],
            other[
                1
            ]
        ),
        math.max(
            one[
                2
            ],
            other[
                2
            ]
        ),
        math.max(
            one[
                3
            ],
            other[
                3
            ]
        ),
        math.min(
            one[
                4
            ],
            other[
                4
            ]
        ),
        math.min(
            one[
                5
            ],
            other[
                5
            ]
        ),
        math.min(
            one[
                6
            ],
            other[
                6
            ]
        ),
    }
end

local box_volume = function (
    box
)
    local x = box[
        4
    ] - box[
        1
    ]
    if 0 > x then
        return 0
    end
    local y = box[
        5
    ] - box[
        2
    ]
    if 0 > y then
        return 0
    end
    local z = box[
        6
    ] - box[
        3
    ]
    if 0 > z then
        return 0
    end
    return x * y * z
end

local new_nodemap = function (
)
    local nodemap = {
        data = {
        },
    }
    nodemap.add = function (
        self,
        key,
        value
    )
        self.data[
            #self.data + 1
        ] = {
            key = key,
            value = value,
        }
    end
    nodemap.get_nearest = function (
        self,
        requested
    )
        local nearest_distance = math.huge
        local nearest_value
        for _, entry in pairs(
            self.data
        ) do
            local key = entry.key
            local value = entry.value
            local distance = 0
            for k, v in pairs(
                requested
            ) do
                distance = distance + (
                    key[
                        k
                    ] - v
                ) *(
                    key[
                        k
                    ] - v
                )
            end
            if distance < nearest_distance then
                nearest_distance = distance
                nearest_value = value
            end
        end
        return nearest_value
    end
    return nodemap
end

local load_nodemap = function (
    filename
)
    local nodemap = new_nodemap(
    )
    local map_file = io.open(
        filename
    )
    if map_file then
        local data = map_file:read(
            "*all"
        )
        map_file:close(
        )
        for line in data:gmatch(
            "([^\n]*)\n?"
        ) do
            if not line:match(
                "^ *#"
            ) then
                local columns = {
                }
                for column in line:gmatch(
                    "[^ ]+"
                ) do
                    columns[
                        #columns + 1
                    ] = column
                end
                if columns[
                    4
                ] then
                    nodemap:add(
                        {
                            r = columns[
                                2
                            ],
                            g = columns[
                                3
                            ],
                            b = columns[
                                4
                            ],
                        },
                        minetest.get_content_id(
                            columns[
                                1
                            ]
                        )
                    )
                end
            end
        end
    end
    return nodemap
end

local new_tablemap = function (
    map_scaling
)
    local tablemap = new_nodemap(
    )
    tablemap.map_scaling = map_scaling
    tablemap.add = function (
        self,
        key,
        value
    )
        local keys = {
        }
        for k, _ in pairs(
            key
        ) do
            keys[
                #keys + 1
            ] = k
        end
        table.sort(
            keys
        )
        local map_key = ""
        for _, k in pairs(
            keys
        ) do
            map_key = map_key .. k .. "v" .. math.round(
                self.map_scaling * key[
                    k
                ]
            )
        end
        self.data[
            map_key
        ] = {
            key = key,
            value = value,
        }
    end
    return tablemap
end

local new_boxmap = function (
    resolution,
    map_scaling
)
    local boxmap = new_tablemap(
        map_scaling
    )
    boxmap.resolution = resolution
    boxmap.split_boxes = split_boxes(
        resolution
    )
    boxmap.add_coverage = function (
        self,
        collision_box,
        value
    )
        local coverage = {
        }
        for k, split in pairs(
            self.split_boxes
        ) do
            local covered = 0
            for _, node in pairs(
                collision_box
            ) do
                covered = covered + (
                    box_volume(
                        box_intersect(
                            node,
                            split
                        )
                    ) / box_volume(
                        split
                    )
                )
            end
            coverage[
                k
            ] = covered
        end
        self:add(
            coverage,
            value
        )
    end
    boxmap.add_node = function (
        self,
        nodename
    )
        local registration = minetest.registered_nodes[
            nodename
        ]
        local collision_box = registration.collision_box
        local fixed
        if collision_box then
            fixed = collision_box.fixed
        else
            fixed = {
                -0.5,
                -0.5,
                -0.5,
                0.5,
                0.5,
                0.5,
            }
        end
        if "table" ~= type(
            fixed[
                1
            ]
        ) then
            fixed = {
                fixed
            }
        end
        if "facedir" == registration.paramtype2 then
            for facedir = 0, 23 do
                local facing = {
                }
                for k, v in pairs(
                    fixed
                ) do
                    facing[
                        k
                    ] = facedir_box(
                        v,
                        facedir
                    )
                end
                self:add_coverage(
                    facing,
                    {
                        node = minetest.get_content_id(
                            nodename
                        ),
                        facedir = facedir,
                    }
                )
            end
        else
            self:add_coverage(
                fixed,
                {
                    node = minetest.get_content_id(
                        nodename
                    ),
                    facedir = 0,
                }
            )
        end
    end
    return boxmap
end

local load_boxmap = function (
    resolution,
    map_scaling,
    filename
)
    local boxmap = new_boxmap(
        resolution,
        map_scaling
    )
    local map_file = io.open(
        filename
    )
    if map_file then
        local data = map_file:read(
            "*all"
        )
        map_file:close(
        )
        for line in data:gmatch(
            "([^\n]*)\n?"
        ) do
            if "" ~= line then
                boxmap:add_node(
                    line
                )
            end
        end
    end
    return boxmap
end

local new_voxelmap = function (
)
    local voxelmap = {
        data = {
        },
        min_x = math.huge,
        max_x = -math.huge,
        min_y = math.huge,
        max_y = -math.huge,
        min_z = math.huge,
        max_z = -math.huge,
    }
    voxelmap.get_min = function (
        self
    )
        return {
            x = self.min_x,
            y = self.min_y,
            z = self.min_z,
        }
    end
    voxelmap.get_max = function (
        self
    )
        return {
            x = self.max_x,
            y = self.max_y,
            z = self.max_z,
        }
    end
    voxelmap.add = function (
        self,
        x,
        y,
        z,
        value
    )
        if self.min_x > x then
            self.min_x = x
        end
        if self.min_y > y then
            self.min_y = y
        end
        if self.min_z > z then
            self.min_z = z
        end
        if self.max_x < x then
            self.max_x = x
        end
        if self.max_y < y then
            self.max_y = y
        end
        if self.max_z < z then
            self.max_z = z
        end
        if not self.data[
            x
        ] then
            self.data[
                x
            ] = {
            }
        end
        local plane = self.data[
            x
        ]
        if not plane[
            y
        ] then
            plane[
                y
            ] = {
            }
        end
        local line = plane[
            y
        ]
        line[
            z
        ] = value
    end
    return voxelmap
end

local load_box_voxelmap = function (
    boxmap,
    filename
)
    local voxelmap = new_voxelmap(
    )
    local voxel_file = io.open(
        filename
    )
    local merged = {
    }
    if voxel_file then
        local data = voxel_file:read(
            "*all"
        )
        voxel_file:close(
        )
        for line in data:gmatch(
            "([^\n]*)\n?"
        ) do
            local columns = {
            }
            for column in line:gmatch(
                "[^ ]+"
            ) do
                columns[
                    #columns + 1
                ] = column + 0
            end
            if columns[
                6
            ] then
                local micro_x = columns[
                    1
                ] % boxmap.resolution
                local micro_y = columns[
                    2
                ] % boxmap.resolution
                local micro_z = columns[
                    3
                ] % boxmap.resolution
                local macro_x = (
                    columns[
                        1
                    ] - micro_x
                ) / boxmap.resolution
                local macro_y = (
                    columns[
                        2
                    ] - micro_y
                ) / boxmap.resolution
                local macro_z = (
                    columns[
                        3
                    ] - micro_z
                ) / boxmap.resolution
                local key = "x" .. macro_x .. "y" .. macro_y .. "z" .. macro_z
                local entry = merged[
                    key
                ]
                if not entry then
                    entry = {
                        pos = {
                            x = macro_x,
                            y = macro_y,
                            z = macro_z,
                        },
                        areas = {
                        },
                    }
                    for x = 1, boxmap.resolution do
                        for y = 1, boxmap.resolution do
                            for z = 1, boxmap.resolution do
                                entry.areas[
                                    "x" .. x .. "y" .. y .. "z" .. z
                                ] = 0
                            end
                        end
                    end
                end
                entry.areas[
                    "x" .. (
                        micro_x + 1
                    ) .. "y" .. (
                        micro_y + 1
                    ) .. "z" .. (
                        micro_z + 1
                    )
                ] = 1
                merged[
                    key
                ] = entry
            end
        end
    end
    for _, entry in pairs(
        merged
    ) do
        voxelmap:add(
            entry.pos.x,
            entry.pos.y,
            entry.pos.z,
            boxmap:get_nearest(
                entry.areas
            )
        )
    end
    return voxelmap
end

local load_voxelmap = function (
    nodemap,
    filename
)
    local voxelmap = new_voxelmap(
    )
    local voxel_file = io.open(
        filename
    )
    if voxel_file then
        local data = voxel_file:read(
            "*all"
        )
        voxel_file:close(
        )
        for line in data:gmatch(
            "([^\n]*)\n?"
        ) do
            local columns = {
            }
            for column in line:gmatch(
                "[^ ]+"
            ) do
                columns[
                    #columns + 1
                ] = column + 0
            end
            if columns[
                6
            ] then
                voxelmap:add(
                    columns[
                        1
                    ],
                    columns[
                        2
                    ],
                    columns[
                        3
                    ],
                    nodemap:get_nearest(
                        {
                            r = columns[
                                4
                            ],
                            g = columns[
                                5
                            ],
                            b = columns[
                                6
                            ],
                        }
                    )
                )
            end
        end
    end
    return voxelmap
end

minetest.register_chatcommand(
    "place_box_voxel",
    {
        params = "<boxmap> <voxelmap>",
        description = "Place a voxel object from file <voxelmap> using shapes from nodes specified in file <boxmap>",
        privs = {
            server = true,
        },
        func = function (
            player_name,
            param
        )
            local arguments = {
            }
            for argument in param:gmatch(
                "[^ ]+"
            ) do
                arguments[
                    #arguments + 1
                ] = argument
            end
            if 2 > #arguments then
                minetest.chat_send_player(
                    player_name,
                    "voxeload: too few parameters given"
                )
                return
            end
            if 2 < #arguments then
                minetest.chat_send_player(
                    player_name,
                    "voxeload: too many parameters given"
                )
                return
            end
            local boxmap = load_boxmap(
                3,
                16,
                minetest.get_modpath(
                    modname
                ) .. "/" .. arguments[
                    1
                ]
            )
            if not boxmap then
                minetest.chat_send_player(
                    player_name,
                    "voxeload: could not load boxmap from file " .. arguments[
                        1
                    ]
                )
                return
            end
            local voxelmap = load_box_voxelmap(
                boxmap,
                minetest.get_modpath(
                    modname
                ) .. "/" .. arguments[
                    2
                ]
            )
            if not voxelmap then
                minetest.chat_send_player(
                    player_name,
                    "voxeload: could not load voxels from file " .. arguments[
                        2
                    ]
                )
                return
            end
            local manipulator = minetest.get_voxel_manip(
            )
            if not manipulator then
                minetest.chat_send_player(
                    player_name,
                    "voxeload: could not initialize voxel manipulator"
                )
                return
            end
            local player = minetest.get_player_by_name(
                player_name
            )
            local base_pos = player:get_pos(
            )
            base_pos = {
                x = math.floor(
                    base_pos.x
                ),
                y = math.floor(
                    base_pos.y
                ),
                z = math.floor(
                    base_pos.z
                ),
            }
            local vmin = voxelmap:get_min(
            )
            local vmax = voxelmap:get_max(
            )
            local pmin, pmax = manipulator:read_from_map(
                vector.add(
                    base_pos,
                    vmin
                ),
                vector.add(
                    base_pos,
                    vmax
                )
            )
            local area = VoxelArea(
                pmin,
                pmax
            )
            local map_data = manipulator:get_data(
            )
            local param_data = manipulator:get_param2_data(
            )
            for x, plane in pairs(
                voxelmap.data
            ) do
                for y, line in pairs(
                    plane
                ) do
                    for z, value in pairs(
                        line
                    ) do
                        local pos = {
                            x = x,
                            y = y,
                            z = z,
                        }
                        local absolute = vector.add(
                            base_pos,
                            pos
                        )
                        map_data[
                            area:indexp(
                                absolute
                            )
                        ] = value.node
                        param_data[
                            area:indexp(
                                absolute
                            )
                        ] = value.facedir
                    end
                end
            end
            manipulator:set_data(
                map_data
            )
            manipulator:set_param2_data(
                param_data
            )
            manipulator:calc_lighting(
            )
            manipulator:update_liquids(
            )
            manipulator:write_to_map(
            )
        end,
    }
)

minetest.register_chatcommand(
    "place_voxel",
    {
        params = "<nodemap> <voxelmap>",
        description = "Place a voxel object from file <voxelmap> with colors mapped to nodes as specified in the file <nodemap>",
        privs = {
            server = true,
        },
        func = function (
            player_name,
            param
        )
            local arguments = {
            }
            for argument in param:gmatch(
                "[^ ]+"
            ) do
                arguments[
                    #arguments + 1
                ] = argument
            end
            if 2 > #arguments then
                minetest.chat_send_player(
                    player_name,
                    "voxeload: too few parameters given"
                )
                return
            end
            if 2 < #arguments then
                minetest.chat_send_player(
                    player_name,
                    "voxeload: too many parameters given"
                )
                return
            end
            local nodemap = load_nodemap(
                minetest.get_modpath(
                    modname
                ) .. "/" .. arguments[
                    1
                ]
            )
            if not nodemap then
                minetest.chat_send_player(
                    player_name,
                    "voxeload: could not load nodemap from file " .. arguments[
                        1
                    ]
                )
                return
            end
            local voxelmap = load_voxelmap(
                nodemap,
                minetest.get_modpath(
                    modname
                ) .. "/" .. arguments[
                    2
                ]
            )
            if not voxelmap then
                minetest.chat_send_player(
                    player_name,
                    "voxeload: could not load voxels from file " .. arguments[
                        2
                    ]
                )
                return
            end
            local manipulator = minetest.get_voxel_manip(
            )
            if not manipulator then
                minetest.chat_send_player(
                    player_name,
                    "voxeload: could not initialize voxel manipulator"
                )
                return
            end
            local player = minetest.get_player_by_name(
                player_name
            )
            local base_pos = player:get_pos(
            )
            base_pos = {
                x = math.floor(
                    base_pos.x
                ),
                y = math.floor(
                    base_pos.y
                ),
                z = math.floor(
                    base_pos.z
                ),
            }
            local vmin = voxelmap:get_min(
            )
            local vmax = voxelmap:get_max(
            )
            local pmin, pmax = manipulator:read_from_map(
                vector.add(
                    base_pos,
                    vmin
                ),
                vector.add(
                    base_pos,
                    vmax
                )
            )
            local area = VoxelArea(
                pmin,
                pmax
            )
            local map_data = manipulator:get_data(
            )
            for x, plane in pairs(
                voxelmap.data
            ) do
                for y, line in pairs(
                    plane
                ) do
                    for z, value in pairs(
                        line
                    ) do
                        local pos = {
                            x = x,
                            y = y,
                            z = z,
                        }
                        local absolute = vector.add(
                            base_pos,
                            pos
                        )
                        map_data[
                            area:indexp(
                                absolute
                            )
                        ] = value
                    end
                end
            end
            manipulator:set_data(
                map_data
            )
            manipulator:calc_lighting(
            )
            manipulator:update_liquids(
            )
            manipulator:write_to_map(
            )
        end,
    }
)
