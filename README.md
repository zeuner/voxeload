# voxeload

`voxeload` allows to place voxel data into Minetest by mapping colors or
local shapes to nodes.

## usage

`voxeload` expects a nodemap mapping RGB colors to Minetest nodes, and
a voxel file in XYZRGB format. Instead of the nodemap, a boxmap can be
used which specifies nodes with different collision box shapes.

The nodemap is a text file consisting of lines that contain 4 space-separated
values: a node name, and values for three color components in RGB color space
ranging from 0 to 255. It's the same format also understood by `voxelizer`
(https://github.com/appgurueu/voxelizer).

The boxmap is a txt file consisting only of lines containing one node name.
Color is disregarded here. It works best with boxes of different shapes,
defined by their collision box.

The voxel file consists of lines that contain 6 space-separated values, the
three coordinates X, Y, Z as integers, followed by the three color components
in RGB color space ranging from 0 to 255. It can be written e.g. by `voxel-io`
(https://github.com/Eisenwave/voxel-io).

### /place_voxel \<nodemap\> \<voxelmap\>

Place a voxel object from file \<voxelmap\> with colors mapped to nodes as
specified in the file \<nodemap\>

### /place_box_voxel \<boxmap\> \<voxelmap\>

Place a voxel object from file \<voxelmap\> using shapes from nodes
specified in file \<boxmap\>
